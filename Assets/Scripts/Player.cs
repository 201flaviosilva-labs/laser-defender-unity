﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Config
    [Header("Player")]
    [SerializeField] float speed = 10f;
    [SerializeField] float padding = 1f;
    [SerializeField] float health;

    [Header("Laser")]
    [SerializeField] GameObject laserPrefab;
    [SerializeField] float laserSpeed = 10f;
    [SerializeField] float fireRate = 0.1f;

    [Header("Audios")]
    [SerializeField] AudioClip shootAudio;
    [SerializeField] AudioClip explosionAudio;

    [Header("Level Scenes")]

    // Internal Vars
    float xMin, xMax;
    float yMin, yMax;
    GameSession gameSession;
    Coroutine fireCoroutine;

    // Start is called before the first frame update
    void Start()
    {
        gameSession = FindObjectOfType<GameSession>();
        health = gameSession.GetHealth();

        SetUpMoveBoundaries();
    }

    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        Vector3 min = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 max = gameCamera.ViewportToWorldPoint(new Vector3(1, 1, 0));

        xMin = min.x + padding;
        xMax = max.x - padding;

        yMin = min.y + padding;
        yMax = max.y - padding;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Fire();
    }

    private void Move()
    {
        float x = transform.position.x + Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float y = transform.position.y + Input.GetAxis("Vertical") * Time.deltaTime * speed;

        float newX = Mathf.Clamp(x, xMin, xMax);
        float newY = Mathf.Clamp(y, yMin, yMax);

        transform.position = new Vector2(newX, newY);
    }

    private void Fire()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            fireCoroutine = StartCoroutine(FireContinuously());
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            StopCoroutine(fireCoroutine);
        }
    }

    IEnumerator FireContinuously()
    {
        while (true)
        {
            yield return new WaitForSeconds(fireRate);

            GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity) as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, laserSpeed);

            AudioSource.PlayClipAtPoint(shootAudio, Camera.main.transform.position, 0.25f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) // Other Game Object
    {
        GameObject gb = collision.gameObject;
        DamageDealer damageDealer = gb.GetComponent<DamageDealer>();
        if (!damageDealer) return;

        gameSession.RemoveHealth(damageDealer.GetDamage());
        damageDealer.Hit();

        health = gameSession.GetHealth();
        if (health <= 0)
        {
            AudioSource.PlayClipAtPoint(explosionAudio, Camera.main.transform.position);
            Destroy(gameObject);
            FindObjectOfType<Level>().LoadGameOver();
        }
    }
}
