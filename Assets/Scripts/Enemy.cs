﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Config")]
    [SerializeField] float health = 100;
    [SerializeField] int scorePoints = 0;

    [Header("Shooting Timer")]
    [SerializeField] float minTimeShoot = 0.5f;
    [SerializeField] float maxTimeShoot = 5f;
    [SerializeField] float countDownShoot;
    [SerializeField] float shootingTimer;

    [Header("Laser/Shoot Config")]
    [SerializeField] GameObject laserPrefab;
    [SerializeField] float laserSpeed = -5f;

    [Header("Particle Explosion")]
    [SerializeField] GameObject particleExplosionPrefab;

    [Header("Audios")]
    [SerializeField] AudioClip shootAudio;
    [SerializeField] AudioClip explosionAudio;

    void Start()
    {
        scorePoints = Random.Range(50, 250);
        shootingTimer = countDownShoot = Random.Range(minTimeShoot, maxTimeShoot);
    }

    void Update()
    {
        countDownShoot -= Time.deltaTime;
        if (countDownShoot <= 0)
        {
            countDownShoot = shootingTimer;
            Fire();
        }
    }

    private void Fire()
    {
        GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity) as GameObject;
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, laserSpeed);

        AudioSource.PlayClipAtPoint(shootAudio, Camera.main.transform.position);
    }

    private void OnTriggerEnter2D(Collider2D collision) // Other Game Object
    {
        GameObject gb = collision.gameObject;
        DamageDealer damageDealer = gb.GetComponent<DamageDealer>();
        if (!damageDealer) return;

        health -= damageDealer.GetDamage();
        damageDealer.Hit();

        // Die
        if (health <= 0)
        {
            FindObjectOfType<GameSession>().AddSore(scorePoints);
            GameObject explosion = Instantiate(particleExplosionPrefab, transform.position, transform.rotation) as GameObject;
            Destroy(explosion, 1f);

            AudioSource.PlayClipAtPoint(explosionAudio, Camera.main.transform.position, 0.5f);

            Destroy(gameObject);
        }
    }
}
