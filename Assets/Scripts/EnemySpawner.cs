﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] List<WaveConfig> waveConfigs;
    [SerializeField] int startingWave = 0;
    [SerializeField] bool isInLoop = true;

    IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(SpawAllWaves());
        } while (isInLoop);
    }

    IEnumerator SpawAllWaves()
    {

        for (int i = startingWave; i < waveConfigs.Count; i++)
        {
            var currentWave = waveConfigs[i];
            yield return StartCoroutine(SpawnAllEnemyesInWave(currentWave));
        }
    }

    IEnumerator SpawnAllEnemyesInWave(WaveConfig waveConfig)
    {

        for (int i = 0; i < waveConfig.GetNumberOfEnemies(); i++)
        {
            float spanTime = waveConfig.GetSpanTime();

            var newEnemy = Instantiate(
                 waveConfig.GetEnemyPrefab(),
                 waveConfig.GetWayPoints()[0].transform.position,
                 Quaternion.identity
             );

            newEnemy.GetComponent<EnemyPathing>().SetWaveConfig(waveConfig);

            yield return new WaitForSeconds(spanTime);
        }
    }
}
