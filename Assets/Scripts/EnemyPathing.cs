﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    WaveConfig waveConfig;
    [SerializeField] List<Transform> wayPoints;
    [SerializeField] int currentPoint = 0;

    void Start()
    {
        wayPoints = waveConfig.GetWayPoints();

        transform.position = wayPoints[0].position;
    }
    public void SetWaveConfig(WaveConfig newWaveConfig)
    {
        waveConfig = newWaveConfig;
    }

    void Update()
    {
        Move();
    }

    private void Move()
    {
        if (currentPoint <= wayPoints.Count - 1)
        {
            float speed = waveConfig.GetSpeed();
            var targetP = wayPoints[currentPoint].position;

            transform.position = Vector2.MoveTowards(transform.position, targetP, speed * Time.deltaTime);
            if (transform.position == targetP) currentPoint++;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
