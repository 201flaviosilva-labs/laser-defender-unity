﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{

    [SerializeField] float delayGameOverTime = 1f;

    public void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("Game");
        GameSession gameSession = FindObjectOfType<GameSession>();

        if (gameSession) gameSession.ResetGame();
    }

    public void LoadGameOver()
    {
        StartCoroutine(DelayGameOver());
    }

    private IEnumerator DelayGameOver()
    {
        yield return new WaitForSeconds(delayGameOverTime);
        SceneManager.LoadScene("Game Over");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
