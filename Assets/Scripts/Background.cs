﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] float speed = 0.005f;
    Material material;
    Vector2 offSet;
    void Start()
    {
        material = GetComponent<Renderer>().material;
        offSet = new Vector2(0f, speed);
    }

    void Update()
    {
        material.mainTextureOffset += offSet * Time.deltaTime;
    }
}
