﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    Text scoreLabel;
    GameSession gameSession;
    void Start()
    {
        scoreLabel = GetComponent<Text>();
        gameSession = FindObjectOfType<GameSession>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreLabel.text = gameSession.GetHealth().ToString();
    }
}
