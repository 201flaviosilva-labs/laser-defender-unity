﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSession : MonoBehaviour
{
    int score = 0;
    float health = 1000;
    void Awake()
    {
        if (FindObjectsOfType<GameSession>().Length > 1) Destroy(gameObject);
        else DontDestroyOnLoad(gameObject);
    }

    public float GetHealth()
    {
        return health;
    }

    public void RemoveHealth(int value)
    {
        health -= value;
    }

    public int GetScore()
    {
        return score;
    }

    public void AddSore(int value)
    {
        score += value;
    }

    public void ResetGame()
    {
        health = 1000;
        score = 0;
    }
}
